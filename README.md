# jogg

jogg library

Original port: http://dmilvdv.narod.ru/Apps/oggvorbis.html

Original code: https://www.xiph.org/

## Cetral Maven Repo

```xml
<dependency>
  <groupId>com.github.axet</groupId>
  <artifactId>jogg</artifactId>
  <version>1.3.3</version>
</dependency>
```

## Android Studio

```gradle
    api 'com.github.axet:jogg:1.3.3'
```
